Source: getmail6
Section: mail
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Sudip Mukherjee <sudipm.mukherjee@gmail.com>
Build-Depends: debhelper-compat (= 13), dh-python, python3-setuptools, python3-all
Standards-Version: 4.7.0
Homepage: https://getmail6.org/
Vcs-Browser: https://salsa.debian.org/python-team/packages/getmail6
Vcs-Git: https://salsa.debian.org/python-team/packages/getmail6.git
Testsuite: autopkgtest-pkg-python

Package: getmail6
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Breaks: getmail (<< 6.7-1)
Replaces: getmail (<< 6.7-1)
Description: mail retriever with support for POP3, IMAP4 and SDPS
 getmail6 is intended as a simple replacement for fetchmail.
 It retrieves mail (either all messages, or only unread messages)
 from one or more POP3/IMAP4/SDPS servers for one or more email
 accounts, and reliably delivers into a qmail-style Maildir, mbox
 file or to a command (pipe delivery) like maildrop or procmail,
 specified on a per-account basis. getmail6 also has support for
 domain (multidrop) mailboxes.
 .
 Supported protocols:
 POP3, POP3-over-SSL, IMAP4, IMAP4-over-SSL, and SDPS mail.
 .
 getmail6 is based on getmail with adaptations for Python3.
