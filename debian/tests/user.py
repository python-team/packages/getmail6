#!/usr/bin/python3

import passlib.hash
import os
import pwd
import random
import string
import subprocess
import sys


def random_string(length):
    '''Return a random string, consisting of ASCII letters, with given
    length.'''

    s = ''
    maxind = len(string.ascii_letters)-1
    for _ in range(length):
        s += string.ascii_letters[random.randint(0, maxind)]
    return s.lower()


def login_exists(login):
    '''Checks whether the given login exists on the system.'''

    try:
        pwd.getpwnam(login)
        return True
    except KeyError:
        return False

login = None

while True:
    login = random_string(8)
    if not login_exists(login):
        break

password = random_string(8)
salt = random_string(2)

crypted = passlib.hash.sha512_crypt.hash(password, rounds=5000)
subprocess.check_call(['useradd', '-p', crypted, '-m', '-s', '/bin/bash', login])
print (password + " " + login)
